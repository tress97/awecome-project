import React from 'react';
import { View, StyleSheet } from 'react-native';


const ImageCard = () => {
    return (
        <View style={{
            flex: 1,
            height: 250,
            backgroundColor: '#E7E0EC',
            borderRadius: 8,
            overflow: 'hidden',
        }}>
        </View>
    );
}
export default ImageCard;