import React from 'react';
import { Text, View, StyleSheet } from 'react-native';


const NewsCard = (props) => {
    return (
        <View style={styles.card} >
            <View style={styles.image} >
            </View>
            <View style={styles.desc} >
                <Text style={styles.headline} > {props.headline} </Text>
                <Text style={styles.support} > {props.support_text} </Text>
            </View>
            <Text style={styles.date} > {props.date} </Text>
        </View>
    );
}

const styles = StyleSheet.create({
  card: {
    display: 'flex',
    width: '100%',
    height: 88,
    flexDirection: 'row',
    columnGap:  16,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 12,
  },
  image: {
    width: 120,
    height: 64,
    backgroundColor: '#E7E0EC',
    borderBottomRightRadius: 4,
    borderTopRightRadius: 4,
    overflow: 'hidden',
  },
  desc: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  headline: {
    color: '#1C1B1F',
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.5,
    fontWeight: '400',
  },
  support: {
    color: '#1C1B1F',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    fontWeight: '400',
  },
  date: {
    color: '#49454F',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    fontWeight: '400',
  },
});

export default NewsCard;