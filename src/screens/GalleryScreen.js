import React from 'react';
import { Text, View, ScrollView, StatusBar, StyleSheet } from 'react-native';
import { Appbar } from 'react-native-paper';

/* Component import */
import ImageCard from '../components/ImageCard.js';


const GalleryScreen = () => {
    return (
        <View style={{ backgroundColor: '#fdfafe' }}>
            <StatusBar backgroundColor="#fdfafe" animated={true} barStyle='dark-content' />
            <Appbar.Header>
                <Appbar.Content title="Gallery" />
                <Appbar.Action icon="filter-variant" onPress={() => {}} />
                <Appbar.Action icon="dots-vertical"  onPress={() => {}} />
            </Appbar.Header>
            <ScrollView style={styles.scroll_view} >
                <View style={styles.hor_layout}>
                     <View style={styles.ver_layout}>
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                     </View>
                     <View style={styles.ver_layout}>
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                        <ImageCard />
                     </View>
                </View>
                <View style={styles.spacer}></View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    scroll_view: {
        backgroundColor: '#fdfafe',
        height: '100%',
        marginBottom: 80,
    },
    spacer: {
        height: 80,
    },
    ver_layout: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        rowGap: 8,
    },
    hor_layout: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        columnGap:  8,
        paddingLeft: 8,
        paddingRight: 8,
    },
});

export default GalleryScreen;