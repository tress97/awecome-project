import React from 'react';
import { Text, View, ScrollView, StatusBar, StyleSheet } from 'react-native';
import { TextInput, Button } from 'react-native-paper';


const LogInScreen = () => {
    return (
        <View style={styles.scroll_view}>
            <StatusBar backgroundColor="#fdfafe" animated={true} barStyle='dark-content' />
            <View style={styles.form}>
                <Text style={styles.headline}>Log in</Text>
                <View style={styles.inputs_group}>
                    <View style={styles.items_group}>
                        <TextInput
                            mode="flat"
                            label="First name"
                            placeholder="Your first name..."
                        />
                        <TextInput
                            mode="flat"
                            label="Last Name"
                            placeholder="Your last name..."
                        />
                    </View>
                    <TextInput
                        mode="flat"
                        label="E-Mail"
                        placeholder="Gmail or other mail..."
                        inputMode="email"
                    />
                    <View style={styles.items_group}>
                        <TextInput
                            mode="flat"
                            label="Password"
                            placeholder="Password..."
                            right={<TextInput.Affix text="/20" />}
                            inputMode="email"
                            secureTextEntry={true}
                        />
                        <TextInput
                            mode="flat"
                            label="Password"
                            placeholder="Password retry..."
                            right={<TextInput.Affix text="/20" />}
                            inputMode="email"
                            secureTextEntry={true}
                        />
                    </View>
                </View>
                <View style={styles.items_group}>
                    <Button mode="contained" onPress={() => console.log('Pressed')}>
                        Continue
                    </Button>
                    <Text style={styles.alternative_login}>Already have an account?</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    scroll_view: {
        backgroundColor: '#fdfafe',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
    },
    form: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingRight: 16,
        paddingLeft: 16,
        paddingTop: 64,
        paddingBottom: 64,
        rowGap:  60,
    },
    headline: {
        color: '#6750A4',
        fontSize: 32,
        lineHeight: 40,
        letterSpacing: 0,
        fontWeight: '400',
    },
    items_group: {
        width: '100%',
        rowGap:  12,
    },
    inputs_group: {
        width: '100%',
        rowGap:  36,
    },
    alternative_login: {
        width: '100%',
        textAlign: 'center',
        color: '#6750A4',
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0.1,
        fontWeight: '500',
    },
});

export default LogInScreen;