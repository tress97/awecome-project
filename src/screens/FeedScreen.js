import React from 'react';
import { Text, View, ScrollView, StatusBar, StyleSheet } from 'react-native';
import { Appbar } from 'react-native-paper';

/* Component import */
import NewsCard from '../components/NewsCard.js';


const FeedScreen = () => {
    return (
        <View style={{ backgroundColor: '#fdfafe' }}>
            <StatusBar backgroundColor="#fdfafe" animated={true} barStyle='dark-content' />
            <Appbar.Header>
                <Appbar.Content title="News" />
                <Appbar.Action icon="filter-variant" onPress={() => {}} />
                <Appbar.Action icon="dots-vertical"  onPress={() => {}} />
            </Appbar.Header>
            <ScrollView style={styles.scroll_view} >
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <NewsCard headline='Headline' support_text='Support text' date='Date' />
                <View style={styles.spacer}></View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    scroll_view: {
        backgroundColor: '#fdfafe',
        height: '100%',
        marginBottom: 80,
    },
    spacer: {
        height: 80,
    },
});
export default FeedScreen;