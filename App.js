import * as React from 'react';
import { Text, NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaProvider } from 'react-native-safe-area-context';

/* screens */
import FeedScreen from './src/screens/FeedScreen.js';
import GalleryScreen from './src/screens/GalleryScreen.js';
import LogInScreen from './src/screens/LogInScreen.js';


const Tab = createMaterialBottomTabNavigator();

function App() {
  return (
    <SafeAreaProvider>
        <NavigationContainer>
            <Tab.Navigator screenOptions={{ headerShown: false }} barStyle={{ backgroundColor: '#fdfafe' }} >
                <Tab.Screen
                    name='Feed'
                    component={FeedScreen}
                    options={{
                        tabBarLabel: 'Feed',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="newspaper-variant" color={color} size={24} />
                        ),
                    }}
                />
                <Tab.Screen
                    name='Gallery'
                    component={GalleryScreen}
                    options={{
                        tabBarLabel: 'Gallery',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="image-multiple" color={color} size={24} />
                        ),
                    }}
                />
                <Tab.Screen
                    name='LogIn'
                    component={LogInScreen}
                    options={{
                        tabBarLabel: 'Guest',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="account" color={color} size={24} />
                        ),
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default App;
